
# here for backward compatibility, use `h5cross` instead

from h5cross.same_nob import h5_same
from h5cross.same_nob import dict_same
from h5cross.same_nob import h5dict_safe


__all__ = ["h5_same", "dict_same", "h5dict_safe"]
