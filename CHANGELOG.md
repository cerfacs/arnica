# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).



## [1.14.1] 2025 / 02 / 18


### Fixed

- Adding a missing `__init__.py` in folder `arnica/plots`

## [1.14.0] 2025 / 01 / 30


### Added

- `HoverItems` class to draw on pyplot axes with interactive balloons
- `structarr_io` facility to convert dict arrrays into structured arrays and back
- `brokenlines_actions` geometric utilities for 2d interactive plotting

### Changed

- `test_common` is now splitted in more specialized tests

### Removed

- Radiation solver, not used anymore, kept in the git

## [1.13.2] 2024 / 01 / 22


### Fixed

 - Comma separator moved from , to ; in `datadict2file`.

## [1.13.1] 2023 / 03 / 21

### Added

 - in phys, `h_colburn_detailed` returns also stanton, reynolds and prandtl
 
### Changed

 - the colburn turbulent correlation is now clip low reynolds to avoid crazy values in laminar zones

## [1.13.0] 2022 / 02 / 22

### Added

 - setup.cfg file

### Changed

 - Add `gain` parameter to 'mask_boundary_layer()' to sacle up or down the BL detected.
 - Avoid NaN ins renormalization of vectors `renormalize`.
 - Cli update to show version
 
## [1.12.0] 2020 / 01 / 20


### Added

Add 'mask_boundary_layer()' to search boundary layers features in numpy arrays


## [1.11.1] 2021 / 09 / 14

### Changed

* `plotille` package is now optional.

### Deprecated

* `showy`-related functions (have their own package know)
* `arnica.utils.same_nob`: moved to `h5cross`

### Removed

* Removed previously deprecated `arnica.utils.temporal_analysis_tools`: use `satis`



## [1.11.0] 2021 / 09 / 14

### Added

Add Stanton thermal correlation in thermal utilities

### Changed

Rely on Kokiy instead of arnica axishells.



## [1.10.1] 2021 / 06 / 11

### Added

Add scaling parameter to former Axishell feature (use not recommended anyway)

### Fixed

Tangent component of rad_theta_components(self, vect_y, vect_z) is fixed (sign changed)



## [1.10.0] 2021 / 05 / 10

### Added

 - shell.rad_theta_components()
 - CLI to plot CSV files without graphical display (plotille)



## [1.9.0] 2021 / 03 / 19

### Added

 - showy "compare" method
 - AxiPointCloud class to move points clouds around
 - Optional argument `extra_vars` to `AVBPAsPointCloud.load_avgsol()` method
 - Shell method `invert_normals` has been added

### Changed

 - Removed pandas and mplcursors dependencies from arnica [`showy.py`, `lay_and_temp_manager.py`]
 - Showy is now using color-blind proof palette.

### Fixed

 - Fixed `angle_btw_vects` function in `vector_actions` module
 - Forced `bnode_lidx` and `bnode_gnode` arrays to 'int' type in `AVBPAsPointCloud`

### Deprecated

- data_avbp_as_ptcloud is deprecated, use version from pyavbp




## [1.8.0] 2020 / 07 / 30

### Added
 -  `intersect_plan_line` function in directed_projection



## [1.7.2] 2020 / 06 / 16

### Added
 -  `h5_same`/`dict_same` tool added for dictionaries and h5 comparisons.


### Fixed
 - bugfix to make `h5_same` tool more flexible on numpy types, deprecation from h5py removed.



## [1.6.0] 2020 / 06 / 16

### Added
 - `CartShell3`a point baset shell defintion
 - `rotate_vect_around_axis` in `vector_actions.py` with test

### Changed
 - In `directed_projection.py` switch from `proj_axis` optional variable to `project` bool



## [1.5.4] 2020 / 03 / 24

### Changed
 - Setup.py version

### Fixed
 - Adapted dump\_dict\_1d\_nparrays and dump\_0d to openpyxl package presence or not

### Deprecated
 - 'pandas' package in showy 
 - dump\_dict2xmdf() (use dump\_dico\_2d\_nparrays() instead)



## [1.5.3] 2020 / 03 / 16

### Changed
 - Setup.py version

### Fixed
 - Removed np.ravel from dump\_dict2xmdf to recover connectivity



## [1.5.2] 2020 / 03 / 16

### Added
 - Add plot\_quat2tri.py to arnica.utils to plot cartesian mesh as triangular mesh with field.

### Changed
 - Setup.py version



## [1.5.1] 2020 / 03 / 15

### Changed
 - Setup.py version

### Fixed
 - In datadict2file fixed dump\_dict2xmdf



## [1.5.0] 2020 / 03 / 13



## [1.5.0] 2020 / 03 / 13

### Added
 - Add sample\_curve\_from\_cloud.py with docstring, pylint (10) and test (98%)
 - Add cart\_to\_cyl to vector\_actions.py with docstring, pylint (10) and test
 - Add test for dilate\_vect\_around\_x
 - Add clip\_cloud\_by\_bounds to cloud2cloud.py, with docstring, pylint(10) and test 
 - conftest.py to add fixture datadir in pytest
 - cyl\_to\_cart in vector\_actions.py
 - README.md in utils with an example of use of temporal_analysis_tools
 - In temporal\_analysis\_tools : Add of interval convergence cartography
 - function phi_from_far in yk_from_phi script
 - Add Shell parent class, along with children AxiShell and CartShell classes

### Changed
 - move to src structure
 - Changed assert functions to numpy test functions to use tolerances
 - Changed ks-test function to allow it to manage 1D vectors
 - Removed TeX dependency form plot\_ave\_with\_interval

### Fixed
 - In wall_thermal_equilibrium : while loop fixed

### Deprecated
 - axishell.py : Use new axishell\_2.py or cartshell\_2.py



## [1.3.4] 2020 / 01 / 08
 
### Added
 add function to plot density of points on meshes 



## [1.3.3] 2019 / 12 / 05

### Added
 - tool **yk_from_phi**
 - examples folder

### Changed
 - Documentation using sphinx
 - call to showy with showy instead of display

### Deprecated
 - solver2d, now forked the **calcifer** repository



## [1.3.2] 2019 / 11 / 25

### Changed
 - Documentation layout using sphinx
 - pages step in the CI
 - addition on Sphinx content in the sub-packages
 
### Fixed
 - dependency to **mplcursors** for showy

### Deprecated
 - solver2d, now forked the **calcifer** repository



## [1.3.1] 2019 / 11 / 15
 
### Fixed
 - A tool called **data_avbp_as_ptcloud** was deleted so we re-introduce it.



## [1.3.0] 2019 / 11 / 14

### Added
- A tool named **showy** --> Makes plots with an easy handling 

### Deprecated
 - No more dependance with **sympy**
 - No more dependancies with **(py)AVBP**
