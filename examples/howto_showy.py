"""Demonstration on the use of showy.

Showy is just a way to store matplotlib
 subplots data and layouts into dictionnaries"""

import numpy as np
from arnica.utils.showy import showy


def showy_demo_plain():
    """Damo of a simplified set of graphs with showy"""
    data = dict()
    data["time"] = np.linspace(0, 0.1, num=256)

    data["sine_10"] = np.cos(data["time"] * 10 * 2 * np.pi)
    data["sine_30"] = np.cos(data["time"] * 30 * 2 * np.pi)
    data["sine_100"] = np.cos(data["time"] * 100 * 2 * np.pi)
    data["sine_100p1"] = 1. + np.cos(data["time"] * 100 * 2 * np.pi)

    # Creating a template
    layout = {
        "title": "Example",
        "graphs": [
            {
                "curves": [{"var": "sine_10"}],
                "x_var": "time",
                "y_label": "Fifi [mol/m³/s]",
                "x_label": "Time [s]",
                "title": "Sinus of frquency *"
            },
            {
                "curves": [{"var": "sine_30"}],
                "x_var": "time",
                "y_label": "Riri [Hz]",
                "x_label": "Time [s]",
                "title": "Second graph"
            },
            {
                "curves": [
                    {
                        "var": "sine_100",
                        "legend": "origin",
                    },
                    {
                        "var": "sine_100p1",
                        "legend": "shifted",
                    }
                ],
                "x_var": "time",
                "y_label": "Loulou [cow/mug]",
                "x_label": "Time [s]",
                "title": "Third graphg"
            }
        ],
        "figure_structure": [3, 1],
        "figure_dpi": 92.6
    }

    # Displaying the data described in the new created layout
    showy(layout, data)


def showy_demo_wildcards():
    """Damo of a simplified set of graphs with showy"""
    data = dict()
    data["time"] = np.linspace(0, 0.1, num=256)

    freq = 10.
    for freq in np.linspace(10, 20, num=9):
        data["sine_" + str(freq)] = np.cos(data["time"]*freq*2*np.pi)

    # Creating a template
    template = {
        "title": "Example",
        "graphs": [{
            "curves": [{"var": "sine_*"}],
            "x_var": "time",
            "y_label": "Sine [mol/m³/s]",
            "x_label": "Time [s]",
            "title": "Sinus of frquency *"
        }],
        "figure_structure": [3, 3],
        "figure_dpi": 92.6
    }

    showy(template, data)

showy_demo_plain()
#showy_demo_wildcards()