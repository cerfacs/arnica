"""Demonstration on the use of showy.

Showy is just a way to store matplotlib
 subplots data and layouts into dictionnaries"""

import numpy as np

from arnica.utils.show_mat import show_mat


def show_mat_plain():
    """Damo of a simplified set of graphs with showy"""

    def samplemat(dims):
        """Make a matrix with all zeros and increasing elements 
        on the diagonal ans last dim"""
        aa = np.zeros(dims)
        for i in range(min(dims)):
            aa[i, :i] = i
        return aa
    # Display matrix
    show_mat(samplemat((15, 15)), "trout")


show_mat_plain()
