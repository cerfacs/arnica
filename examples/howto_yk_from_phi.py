"""Demonstration of Yk_from_phi"""
from arnica.phys.yk_from_phi import yk_from_phi

def howto_yk_from_phi():
    """Test yk_from_phi  on methane and kerozenz"""

    pprint(yk_from_phi(1.0, 1, 4))

    pprint(yk_from_phi(1.0, 10.2, 22.3))

howto_yk_from_phi()