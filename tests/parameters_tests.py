"""define parameters for tests inputs and outputs"""

import os
import inspect

TESTS_ROOT = (inspect.getfile(inspect.currentframe()))
TESTS_ROOT = (os.path.dirname(os.path.abspath(TESTS_ROOT)))

# Input files directory (tests_data)
DATA_DIR = os.path.join(TESTS_ROOT, 'tests_data')

# Output files directory (tests_output)
OUT_DIR = os.path.join(TESTS_ROOT, 'tests_output')