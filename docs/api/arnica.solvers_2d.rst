arnica.solvers\_2d package
==========================

.. automodule:: arnica.solvers_2d
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

arnica.solvers\_2d.boundary module
----------------------------------

.. automodule:: arnica.solvers_2d.boundary
   :members:
   :undoc-members:
   :show-inheritance:

arnica.solvers\_2d.conduction module
------------------------------------

.. automodule:: arnica.solvers_2d.conduction
   :members:
   :undoc-members:
   :show-inheritance:

arnica.solvers\_2d.core\_fd module
----------------------------------

.. automodule:: arnica.solvers_2d.core_fd
   :members:
   :undoc-members:
   :show-inheritance:

arnica.solvers\_2d.radiation module
-----------------------------------

.. automodule:: arnica.solvers_2d.radiation
   :members:
   :undoc-members:
   :show-inheritance:
