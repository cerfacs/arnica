arnica package
==============

.. automodule:: arnica
   :members:
   :undoc-members:
   :show-inheritance:

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   arnica.phys
   arnica.solvers_2d
   arnica.utils

Submodules
----------

arnica.cli module
-----------------

.. automodule:: arnica.cli
   :members:
   :undoc-members:
   :show-inheritance:
