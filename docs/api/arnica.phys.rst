arnica.phys package
===================

.. automodule:: arnica.phys
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

arnica.phys.solid\_material module
----------------------------------

.. automodule:: arnica.phys.solid_material
   :members:
   :undoc-members:
   :show-inheritance:

arnica.phys.thermodyn\_properties module
----------------------------------------

.. automodule:: arnica.phys.thermodyn_properties
   :members:
   :undoc-members:
   :show-inheritance:

arnica.phys.wall\_thermal\_equilibrium module
---------------------------------------------

.. automodule:: arnica.phys.wall_thermal_equilibrium
   :members:
   :undoc-members:
   :show-inheritance:

arnica.phys.yk\_from\_phi module
--------------------------------

.. automodule:: arnica.phys.yk_from_phi
   :members:
   :undoc-members:
   :show-inheritance:
