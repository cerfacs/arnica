.. _header-n0:

.. arnica documentation master file, created by
   sphinx-quickstart on Thu Dec  6 11:21:44 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.



Welcome to arnica's documentation!
==================================

This is the documentation for the *default* branch of ARNICA.

Contents:

.. toctree::
    intro
    ./api/arnica.phys
    ./api/arnica.utils

.. _header-n3:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _external website: https://gitlab.com/cerfacs/arnica
