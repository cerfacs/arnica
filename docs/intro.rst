.. _introduction:

Introduction
============

Arnica is a package of open source python modules developped by CERFACS-Team COOP, as a toolkit for CFD.
This package contains a solver of second partial derivative equations to treat heat conduction and heat radiation problem.
The 2nd order finit difference scheme is used to solve the inside of a computational domain and that of first order for boundaries.
Arnica is able to treat a 2D computational mesh at present.



Composition
===========

- phys: a few test cases for different type of phenomenon
- utils: some tools to facilitate developement/communication with other external applications
- solvers_2d (Deprecated) : modules to solve two dimensional heat conduction and radiation problem 

